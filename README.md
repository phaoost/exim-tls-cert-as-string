# exim-tls-cert-as-string

Provide capability to read TLS private key and certificate from string

By default exim expects a full path to a file for tls_privatekey/tls_certificate option.

```
tls_privatekey = /path/to/keyfile.pem
tls_certificate = /path/to/keyfile.pem
```

This does not allow to perform database lookups for the TLS private key and certificate.

The patch changes this behaviour, so the TLS private key and certificate provided as a string for exim:

```
CONFDIR = /etc/exim4
tls_certificate = ${lookup pgsql{\
                    SELECT tls_cert FROM domains \
                     WHERE domain='${quote_pgsql:$tls_in_sni}'}\
                   {$value}\
                   {${readfile{${expand:CONFDIR/exim.crt}}}}}
tls_privatekey = ${lookup pgsql{\
                   SELECT tls_key FROM domains \
                    WHERE domain='${quote_pgsql:$tls_in_sni}'}\
                  {$value}\
                  {${readfile{${expand:CONFDIR/exim.key}}}}}
```

The above example lookups for TLS private key and certificate from database using TLS SNI domain name.
If no value returned, it reads key from files exim.key/exim.crt in /etc/exim4 directory.

It has been tested with exim 4.92-7~bpo9+1 from ascii-backports on Devuan 2.0 ascii Linux distribution but also should work correctly on Debian Stretch with backports.
Steps that require root privileges have root prompt `#`. Steps to run as unprivileged user have prompt `$`. Consider `user` as unprivileged username in the exampple commands.

Steps to install:

1. Verify repositories. Here is the example /etc/apt/sources.list:

```
~# cat /etc/apt/sources.list
deb http://deb.devuan.org/merged ascii main contrib non-free
deb http://deb.devuan.org/merged ascii-updates main contrib non-free
deb http://deb.devuan.org/merged ascii-proposed-updates main contrib non-free
deb http://deb.devuan.org/merged ascii-security main contrib non-free
deb http://deb.devuan.org/merged ascii-backports main contrib non-free
deb-src http://deb.devuan.org/merged ascii main contrib non-free
deb-src http://deb.devuan.org/merged ascii-updates main contrib non-free
deb-src http://deb.devuan.org/merged ascii-proposed-updates main contrib non-free
deb-src http://deb.devuan.org/merged ascii-security main contrib non-free
deb-src http://deb.devuan.org/merged ascii-backports main contrib non-free
```

2. Install build dependencies for exim:

```
~# apt-get build-dep exim4
```

3. Install exim source package from backports:

```
~$ apt-get -t ascii-backports source exim4
```

4. Download and apply patch:

```
~$ wget https://gitlab.com/phaoost/exim-tls-cert-as-string/blob/master/exim-tls-cert-as-string.patch
~$ cd exim4-4.92
~/exim4-4.92$ patch src/tls_gnu.c < ../exim-tls-cert-as-string.patch
```

5. Build package:


```
~/exim4-4.92$ dpkg-buildpackage -b -rfakeroot -us -uc
```

6. Install patched exim4-daemon-heavy package and it's dependencies to allow database lookups:

```
~# dpkg -i ~user/exim4-daemon-heavy_4.92-7~bpo9+1_amd64.deb ~user/exim4-base_4.92-7~bpo9+1_amd64.deb ~user/exim4-config_4.92-7~bpo9+1_all.deb
~# apt-get -f install
```